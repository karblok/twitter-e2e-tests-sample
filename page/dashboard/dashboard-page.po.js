const waitHelper = require('../../helper/wait-helper');
const helper = require('../../helper/helper');
const consoleLogHelper = require ('../../helper/consolelog-helper');


const dashboardElements = require ('../../page/dashboard/modules/dashboard-elements.mo');

class DashboardPage {

    async addNewPost() {
        await waitHelper.waitTenSecForElementToBeVisible(dashboardElements.newPostInput(), 'Timed out waiting for New post input to be visible');
        await dashboardElements.newPostInput().sendKeys("This is a sample of some tweet");
    }

    async postTweet(){
        await waitHelper.waitTenSecForElementToBeVisible(dashboardElements.tweetButton(), 'Timed out waiting for Tweet button to be visible');
        //Checking both twitter buttons
        await dashboardElements.tweetButton().click();
        await dashboardElements.tweetButton().click();
    }

    async menuGoToListsTab(){
        await waitHelper.waitTenSecForElementToBeVisible(dashboardElements.menuLists(), 'Timed out waiting for Lists tab button to be visible');
        await dashboardElements.menuLists().click();
    }


}
module.exports = new DashboardPage();
