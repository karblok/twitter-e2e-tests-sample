const fs = require('fs');
const fs_extra = require('fs-extra');

const consoleLogHelper = require('./consolelog-helper');
const waitHelper = require('./wait-helper');
// const sharedElements = require('../page/shared/modules/shared-elements.mo');

class Helper {

    isElementDisplayed(elementDisplayed, message) {
        consoleLogHelper.isTrueAssertionLog(elementDisplayed, message);
        return elementDisplayed;
    }

    async isElementNotPresented(present, messageTrue, messageFalse) {
        if (!present) {
            consoleLogHelper.isElementNotPresentedLog(present, messageTrue);
            return true;
        } else {
            consoleLogHelper.isElementNotPresentedLog(present, messageFalse);
            return false;
        }
    }

    async isElementPresented(present, messageTrue, messageFalse) {
        if (present) {
            consoleLogHelper.isElementPresentedLog(present, messageTrue);
            return true;
        } else {
            consoleLogHelper.isElementPresentedLog(present, messageFalse);
            return false;
        }
    }

    async mouseHover(element) {
        await browser.waitForAngular();

        await browser.actions()
            .mouseMove(element)
            .perform();
    }

}

module.exports = new Helper();
