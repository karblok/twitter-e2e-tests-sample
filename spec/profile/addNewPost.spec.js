const dashboardPage = require('../../page/dashboard/dashboard-page.po');

const userData = require('../../data/loginData/userLoginData.td');
const testCaseData = require('../testCaseData/testCaseData.td');
const tweetText = require('../../data/homeData/dashboardData.td');

const helper = require('../../helper/helper');
const loginHelper = require('../../helper/loginHelper');
const consoleLogHelper = require('../../helper/consolelog-helper');
const waitHelper = require('../../helper/wait-helper');

describe('Create new post tests', () => {
    const testCase = testCaseData.TWITTER_02;
    const tweet = tweetText.newTweet;
    const post = tweet.tweetText;

    const EC = protractor.ExpectedConditions;

    beforeAll(async () => {
        global.testHeader = testCase.testHeader;
        browser.ignoreSynchronization = true;
        consoleLogHelper.beforeAllLog();

        const user = userData.testaccount;
        const credentials = {
            username: user.username,
            password: user.password
        };

        await browser.get(browser.baseUrl);
        await loginHelper.login(credentials);

    });

    afterAll(() => {
        consoleLogHelper.afterAllLog();
    });

    it(testCase.testHeader, async () => {
        global.caseId = testCase.testCaseId;

        await dashboardPage.addNewPost();
        await dashboardPage.postTweet();

        expect(element(by.linkText('Your Tweet was sent.')).getTagName()).toBeTruthy();

    })

});
