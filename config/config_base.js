const base = require('./config');

const config = base.config;

config.suites = {
    login: '../spec/login/*',
    dashboard: '../spec/profile/addNewPost.spec.js',
    lists: '../spec/lists/*',
    profile: '../spec/profile/*'
};

exports.config = config;
