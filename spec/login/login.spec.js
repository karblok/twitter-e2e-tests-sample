const loginPage = require('../../page/login/login-page.po');

const userData = require('../../data/loginData/userLoginData.td');
const testCaseData = require('../testCaseData/testCaseData.td');

const consoleLogHelper = require('../../helper/consolelog-helper');

describe('Login to the account', () => {
    const user = userData.testaccount;
    const username = user.username;
    const password = user.password;

    beforeEach(async () => {
        browser.ignoreSynchronization = true;
        await browser.get(browser.baseUrl);
    });

    describe('TWITTER_01', () => {
        const testCase = testCaseData.TWITTER_01;

        beforeAll(() => {
            global.testHeader = testCase.testHeader;
            consoleLogHelper.beforeAllLog();
        });

        afterAll(() => {
            consoleLogHelper.afterAllLog();
        });

        it(testCase.testHeader, async () => {
            global.caseId = testCase.testCaseId;

            await loginPage.enterUsername(username);
            await loginPage.enterPassword(password);
            browser.sleep(5000);

        });
    });

});
