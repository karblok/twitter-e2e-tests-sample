const waitHelper = requireHelper('wait-helper');
const helper = requireHelper('helper');
const consoleLogHelper = requireHelper('consolelog-helper');

const loginPageElements = require('../../page/login/modules/login-elements.mo');

class LoginPage {

    async enterUsername(username) {
        await waitHelper.waitTenSecForElementToBeVisible(loginPageElements.loginInput(), 'Timed out waiting for login input to be visible');
        await loginPageElements.loginInput().sendKeys(username);
        await loginPageElements.passwordInput().click();
        browser.sleep(4000);
    }

    async enterPassword(password) {
        await waitHelper.waitTenSecForElementToBeVisible(loginPageElements.passwordInput(), 'Timed out waiting for password input to be visible');
        await loginPageElements.passwordInput().sendKeys(password);
        await loginPageElements.nextButton().click();
    }

}

module.exports = new LoginPage();
