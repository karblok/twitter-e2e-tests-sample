const waitHelper = require('../../helper/wait-helper');
const helper = require('../../helper/helper');
const consoleLogHelper = require ('../../helper/consolelog-helper');


const listsElements = require ('../../page/lists/modules/lists-elements.mo');

class ListsPage {

    async createNewList() {
        await waitHelper.waitTenSecForElementToBeVisible(listsElements.newListButton(), 'Timed out waiting for New list button to be visible');
        await listsElements.newListButton().click();
        browser.waitForAngularEnabled();
    }

    async newListSettingsStepOne() {
        await waitHelper.waitTenSecForElementToBeVisible(listsElements.newListNameInput(), 'Timed out waiting for New list name input to be visible');
        await listsElements.newListNameInput().sendKeys("This is an example");
        browser.waitForAngularEnabled();
        await listsElements.newListNextButton().click();
    }

    async newListSettingsStepTwo() {
        await waitHelper.waitTenSecForElementToBeVisible(listsElements.newListFinishButton(), 'Timed out waiting for New list finish button to be visible');
        await listsElements.newListFinishButton().click();
        browser.waitForAngularEnabled();
    }

}
module.exports = new ListsPage();
