const navbarElements = require('../page/general/modules/navbar-elements.mo');
const sharedElements = require('../page/shared/modules/shared-elements.mo');
const dashboardElements = require('../page/general/modules/dashboard-elements.mo');

class NavbarHelper {

    async navigateToMyProfile() {
        const accountNavbarButton = await navbarElements.cardsNavButton();
        await accountNavbarButton.click();
    }

    async navigateToDashboard() {
        const accountNavbarButton = await navbarElements.DesktopNavButton();
        await accountNavbarButton.click();
    }

}

module.exports = new NavbarHelper();
