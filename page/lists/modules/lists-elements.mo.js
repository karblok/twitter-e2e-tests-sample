const listsPage = require('../../../data/listsData/listsPageData.td');

class ListsPageElement {

    newListButton() {
        //return $("[class *='css-4rbku5']");
        return element(by.xpath('//*[@id="react-root"]/div/div/div/main/div/div/div/div[1]/div/div[1]/div/div/div/div/div/div[2]/a/div'));
    }

    newListNameInput(){
        return $("[class *='r-30o5oe']");
    }

    newListNextButton(){
        const nextButton = listsPage.newList.nextButton;
        return element(by.cssContainingText("[class *='css-901oao']", nextButton ));
    }

    newListFinishButton(){
        const doneButton = listsPage.newList.finishButton;
        return element(by.cssContainingText("[class *='css-901oao']", doneButton ));
    }

}

module.exports = new ListsPageElement();
