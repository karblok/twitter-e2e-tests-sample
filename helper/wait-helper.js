const ONE_SEC_TIMEOUT = 1000;
const TWO_SEC_TIMEOUT = 2000;
const FIVE_SEC_TIMEOUT = 5000;
const TEN_SEC_TIMEOUT = 10000;
const THIRTY_SEC_TIMEOUT = 30000;

class WaitHelper {
    async waitOneSecForElementToBeVisible(webElement, message) {
        const until = protractor.ExpectedConditions;
        await browser.wait(until.visibilityOf(webElement), ONE_SEC_TIMEOUT, message);
    }

    async waitTwoSecForElementToBeVisible(webElement, message) {
        const until = protractor.ExpectedConditions;
        await browser.wait(until.visibilityOf(webElement), TWO_SEC_TIMEOUT, message);
    }

    async waitFiveSecForElementToBeVisible(webElement, message) {
        const until = protractor.ExpectedConditions;
        await browser.wait(until.visibilityOf(webElement), FIVE_SEC_TIMEOUT, message);
    }

    async waitTenSecForElementToBeVisible(webElement, message) {
        const until = protractor.ExpectedConditions;
        await browser.wait(until.visibilityOf(webElement), TEN_SEC_TIMEOUT, message);
    }

    async waitThirtySecForElementToBeVisible(webElement, message) {
        const until = protractor.ExpectedConditions;
        await browser.wait(until.visibilityOf(webElement), THIRTY_SEC_TIMEOUT, message);
    }

}

module.exports = new WaitHelper();
