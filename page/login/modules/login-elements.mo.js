class LoginPageElement {
    loginInput() {
        return $("[class *='email-input']");
    }

    passwordInput() {
        return element(by.xpath('//*[@id="doc"]/div/div[1]/div[1]/div[1]/form/div[2]/input'));
    }

    nextButton() {
        return $("[class *='EdgeButton']");
    }

    getTwitterLogo() {
        return $("[class *= 'Icon']");
    }

}

module.exports = new LoginPageElement();
