const listsPage = require('../../page/lists/lists-page.po');
const dashboardPage = require('../../page/dashboard/dashboard-page.po');

const userData = require('../../data/loginData/userLoginData.td');
const testCaseData = require('../testCaseData/testCaseData.td');

const helper = require('../../helper/helper');
const loginHelper = require('../../helper/loginHelper');
const consoleLogHelper = require('../../helper/consolelog-helper');
const waitHelper = require('../../helper/wait-helper');

describe('Create new list test', ()=> {
    const testCase = testCaseData.TWITTER_03;

    beforeAll(async () => {
        global.testHeader = testCase.testHeader;
        browser.ignoreSynchronization = true;
        consoleLogHelper.beforeAllLog();

        const user = userData.testaccount;
        const credentials = {
            username: user.username,
            password: user.password
        };

        await browser.get(browser.baseUrl);
        await loginHelper.login(credentials);

    })

    afterAll(() => {
        consoleLogHelper.afterAllLog();
    });

    it(testCase.testHeader, async () =>{
        global.caseId = testCase.testCaseId;

        await dashboardPage.menuGoToListsTab();
        await listsPage.createNewList();
        await listsPage.newListSettingsStepOne();
        browser.waitForAngularEnabled();
        await listsPage.newListSettingsStepTwo();

        expect(element(by.linkText('There aren’t any Tweets in this List')).getTagName()).toBeTruthy();

    })

})

