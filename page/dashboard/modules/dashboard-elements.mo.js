const mainPage = require('../../../data/homeData/homePage.td');

class DashboardPageElement {

    newPostInput() {
        return $("[class *='public-DraftStyleDefault-block']");
    }

    tweetButton(){
        const buttonName = mainPage.tweetSomething.postButton;
        return element(by.cssContainingText("[class *='css-901oao']", buttonName ));
    }

    menuLists(){
        const clickLists = mainPage.leftPanel.listsButton;
        return element(by.cssContainingText("[class *='css-901oao']", clickLists));
    }
}
module.exports = new DashboardPageElement();
