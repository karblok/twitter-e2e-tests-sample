global.redFont = '\x1b[31m';
global.greenFont = '\x1b[32m';
global.whiteFont = '\x1b[37m';

class ConsoleLogHelper {
    beforeAllLog() {
        console.log(`${global.testHeader} | REPORT | START`);
    }

    afterAllLog() {
        console.log(`${global.testHeader} | REPORT | COMPLETED\n`);
    }

    elementNotFound(message) {
        console.log(redFont, `${global.caseId} | OUTPUT  | ${message} | FAILED`);
    }

    isTrueAssertionLog(actual, message) {
        actual === true ?
            console.log(greenFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} | ACTUAL: ${actual} | OK`, whiteFont) :
            console.log(redFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} | ACTUAL: ${actual} | FAILED`, whiteFont);
    }

    isDisplayedLog(isDisplayed, message) {
        isDisplayed === true ?
            console.log(greenFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} to be displayed | ACTUAL: ${isDisplayed} | OK`, whiteFont) :
            console.log(redFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} to be displayed | ACTUAL: ${isDisplayed} | FAILED`, whiteFont);
    }

    areEqualAssertionLog(expected, actual, message) {
        actual === expected ?
            console.log(greenFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} to be ${expected} | ACTUAL: ${actual} | OK`, whiteFont) :
            console.log(redFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} to be ${expected} | ACTUAL: ${actual} | FAILED`, whiteFont);
    }

    areEqualInputLog(inputed, expected, actual, message) {
        actual === expected ?
            console.log(greenFont, `${global.caseId} | OUTPUT | try to input ${inputed} | ` +
                `EXPECTED: ${message} to be ${expected} | ACTUAL: ${actual} | OK`, whiteFont) :
            console.log(greenFont, `${global.caseId} | OUTPUT | try to input ${inputed} | ` +
                `EXPECTED: ${message} to be ${expected} | ACTUAL: ${actual} | OK`, whiteFont);
    }

    isElementNotPresentedLog(actual, message) {
        if (!actual) {
            console.log(greenFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} | PASSED`, whiteFont);
        } else {
            console.log(redFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} | FAILED`, whiteFont);
        }
    }

    isElementPresentedLog(actual, message) {
        if (actual) {
            console.log(greenFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} | PASSED`, whiteFont);
        } else {
            console.log(redFont, `${global.caseId} | OUTPUT | EXPECTED: ${message} | FAILED`, whiteFont);
        }
    }
}

module.exports = new ConsoleLogHelper();
