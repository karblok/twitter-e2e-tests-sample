const loginPage = require('../page/login/login-page.po');

class LoginHelper {

    async login(credentials) {
        await loginPage.enterUsername(credentials.username);
        await loginPage.enterPassword(credentials.password);

    }
}
module.exports = new LoginHelper();
