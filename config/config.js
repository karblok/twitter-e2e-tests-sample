const HtmlReporter = require('protractor-beautiful-reporter');

const env = require('./environment');
const helper = require('../helper/helper');

const helperDirectoy = `${__dirname.replace('config', '')}helper/`;
const linuxHelperDirectory = helperDirectoy.split('\\').join('/');

exports.config = {
    // required variables
    env: env,

    directConnect: true,
    SELENIUM_PROMISE_MANAGER: false,

    seleniumAddress: env.seleniumAddress,
    capabilities: env.chromeCapabilities,

    baseUrl: env.baseUrl.concat(''),

    onPrepare: function () {
        console.log('path: ' + linuxHelperDirectory);
        global.requireHelper = helperFile => require(linuxHelperDirectory + helperFile);


        // Browser configuration

        browser.driver.manage()
            .window()
            .maximize();

        browser.manage()
            .timeouts()
            .pageLoadTimeout(65000);

        browser.manage()
            .timeouts()
            .implicitlyWait(3000);
    }
};
