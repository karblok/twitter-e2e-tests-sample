const servers = require('./servers.json');

module.exports = {
    // The address of a running selenium server.
    seleniumAddress: (process.env.SELENIUM_URL || 'http://localhost:4444/wd/hub'),

    // Capabilities to be passed to the webdriver instance.
    chromeCapabilities: {
        'browserName': (process.env.TEST_BROWSER_NAME || 'chrome'),

        'chromeOptions': {
            args: [
                '--start-maximized'
            ]
        },
        'version': (process.env.TEST_BROWSER_VERSION || 'ANY'),
        'loggingPrefs': {
            'driver': 'WARNING',
            'browser': 'ALL' // "OFF", "SEVERE", "WARNING", "INFO", "CONFIG", "FINE", "FINER", "FINEST", "ALL".
        }
    },

    ieCapabilities: {
        'browserName': (process.env.TEST_BROWSER_NAME || 'internet explorer'),
        'ignoreProtectedModeSettings': true,
        'ieOptions': {
            args: [
                '--start-maximized'
            ]
        },
        'version': (process.env.TEST_BROWSER_VERSION || 'ANY'),
        'loggingPrefs': {
            'driver': 'WARNING',
            'browser': 'ALL' // "OFF", "SEVERE", "WARNING", "INFO", "CONFIG", "FINE", "FINER", "FINEST", "ALL".
        }
    },

    firefoxCapabilities: {
        'browserName': (process.env.TEST_BROWSER_NAME || 'firefox'),
        'ignoreProtectedModeSettings': true,
        'ieOptions': {
            args: [
                '--start-maximized'
            ]
        },
        'version': (process.env.TEST_BROWSER_VERSION || 'ANY'),
        'loggingPrefs': {
            'driver': 'WARNING',
            'browser': 'ALL' // "OFF", "SEVERE", "WARNING", "INFO", "CONFIG", "FINE", "FINER", "FINEST", "ALL".
        }
    },

    // A base URL for your application under test.
    // currentEnvironment: "INT",
    currentEnvironment: "DEV",

    baseUrl: `https://${(process.env.PEKAO_HOST || servers.env_DEV.host1)}`
};
